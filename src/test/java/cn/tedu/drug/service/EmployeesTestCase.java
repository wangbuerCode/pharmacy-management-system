package cn.tedu.drug.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.tedu.drug.entity.Employees;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeesTestCase {
	
	@Autowired
	private IEmployeesService ee;
	
	@Test
	public void changePasswod() {
		try {
		String oldPassword = "321";
		String newPassword = "123";
		Integer uid =13;
		ee.changePassword(oldPassword,newPassword, uid);
		System.err.println("OK");
		}catch( Exception e) {
			System.err.println(e.getClass().getName());
			System.err.println(e.getMessage());
		}
	}
}
