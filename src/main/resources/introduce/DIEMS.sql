/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.5.27 : Database - tedu_diems
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tedu_diems` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tedu_diems`;

/*Table structure for table `t_customer` */

DROP TABLE IF EXISTS `t_customer`;

CREATE TABLE `t_customer` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '客户id',
  `username` varchar(20) NOT NULL COMMENT '客户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(36) DEFAULT NULL COMMENT '盐值',
  `gender` int(11) DEFAULT NULL COMMENT '性别,0-女性，1-男性',
  `age` int(4) DEFAULT NULL COMMENT '年龄',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `avatar` varchar(50) DEFAULT NULL COMMENT '头像',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `cardBank` varchar(50) DEFAULT NULL COMMENT '开户行',
  `card` varchar(20) DEFAULT NULL COMMENT '银行账户',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `t_customer` */

insert  into `t_customer`(`uid`,`username`,`password`,`salt`,`gender`,`age`,`phone`,`email`,`avatar`,`address`,`cardBank`,`card`,`is_delete`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'chenzhifu','65E935BB4857A9B2302A7C8099BA8E36','9EB504C0-C90C-4AC3-9447-97CE1E78D435',1,23,'15133480838','15133480838@qq.com',NULL,'河北省张家口市张北县','石家庄','5647893587954263145',0,'chenzhifu','2019-01-14 20:02:02','chenzhifu','2019-03-14 20:03:17'),(2,'wanglong','9497C4AF1CB573B749ED9F471920231D','1E9C50CD-1D33-40C0-B573-F67BD85389BD',1,NULL,'15133330551','wanglong@qq.com',NULL,NULL,NULL,NULL,0,'wanglong','2019-02-15 15:38:29','wanglong','2019-03-15 15:38:29'),(3,'老中医','FC8BFE23A2E2D5E9D741CC928EE7494A','5C9AC229-C631-48EE-8C49-CFFBD9EA83A9',1,NULL,'12345678910','123456789@qq.com',NULL,NULL,NULL,NULL,0,'老中医','2019-03-15 19:36:10','老中医','2019-03-15 19:36:10'),(4,'三毛','E6D8407A2F0D38F6667F9BAB3876A837','10C5594F-06D6-4E57-A7BE-FD629CB2D5B5',1,26,'13843838438','1234566@qq.com','/upload/4三毛.jpg','北京市高老庄','农行','1234567899876543212',0,'三毛','2019-05-15 19:46:16','三毛','2019-03-15 19:58:40'),(5,'大花','04CAEABD5D754B79F96858711A785C0A','2CB73432-0176-4AA4-8401-0560AF25BC5A',0,NULL,'12345698777','12312312@asd.com',NULL,NULL,NULL,NULL,0,'大花','2019-03-15 20:04:26','大花','2019-03-15 20:04:26'),(6,'wangwu','2F74C3F416FE2D0E527BEB08BE39F49E','FB1EB3A5-B0EE-49B3-A3AA-A8D91C8BF2CD',1,NULL,'98745632112','123546@qq.com',NULL,NULL,NULL,NULL,0,'wangwu','2019-03-15 20:04:58','wangwu','2019-03-15 20:04:58'),(7,'老大','2752CA03DF3C4325ABCC96C8F1A030D3','A1FE2A73-16AA-47F7-97A1-AECE470AF27D',1,NULL,'12331232132','1231312@qq.com',NULL,NULL,NULL,NULL,0,'老大','2019-09-15 20:05:33','老大','2019-03-15 20:05:33'),(8,'zhangsan','70C1E9A32268B42C666385CDBD893C3F','9E191F39-E671-4420-9F7F-191BA87E9776',1,NULL,'12313312231','213131@rr.cpm',NULL,NULL,NULL,NULL,0,'zhangsan','2019-03-15 20:05:57','zhangsan','2019-03-15 20:05:57'),(9,'豆饼','86370792975825F35C25582DBC45932B','C33C8502-1B30-4316-A2C9-8EB9E26AB5CA',1,NULL,'54646515642','123456@ff.com',NULL,NULL,NULL,NULL,0,'豆饼','2019-12-15 20:06:27','豆饼','2019-03-15 20:06:27'),(10,'烦啦烦啦','1CD3F97216B4D9F107981325C685153B','73E6CFA8-C63B-4B24-A351-0B49D24EEFF3',1,NULL,'11111111111','12354545@qq.com',NULL,NULL,NULL,NULL,0,'烦啦烦啦','2019-03-15 20:12:33','烦啦烦啦','2019-03-15 20:12:33');

/*Table structure for table `t_drug` */

DROP TABLE IF EXISTS `t_drug`;

CREATE TABLE `t_drug` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '药品id',
  `drugName` varchar(20) NOT NULL COMMENT '药品名',
  `barCode` varchar(15) NOT NULL COMMENT '条形码',
  `referred` varchar(32) NOT NULL COMMENT '简称',
  `specifications` varchar(20) NOT NULL COMMENT '规格',
  `unit` varchar(20) NOT NULL COMMENT '单位',
  `origin` varchar(150) NOT NULL COMMENT '产地',
  `approvalNumber` varchar(100) NOT NULL COMMENT '批准文号',
  `pleasedTo` float NOT NULL COMMENT '进货价',
  `salesPrice` float NOT NULL COMMENT '售货价',
  `inventory` int(11) NOT NULL COMMENT '库存',
  `totalSales` int(11) NOT NULL COMMENT '销售总量',
  `drugNote` varchar(500) NOT NULL COMMENT '药品备注',
  `category_id` int(11) NOT NULL COMMENT '药品类别id',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `F_category_drug` (`category_id`),
  CONSTRAINT `F_category_drug` FOREIGN KEY (`category_id`) REFERENCES `t_drug_category` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `t_drug` */

insert  into `t_drug`(`id`,`drugName`,`barCode`,`referred`,`specifications`,`unit`,`origin`,`approvalNumber`,`pleasedTo`,`salesPrice`,`inventory`,`totalSales`,`drugNote`,`category_id`,`is_delete`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'西山湖','5469874561','山药','4/2','西山制药公司','河南','789456123',12,20,23,27,'轻微治疗',1,0,'chenzhifu','2019-03-14 20:16:52','chenzhifu','2019-03-14 20:16:52'),(2,'感冒胶囊','123456789','感康','4/2','石家庄','石家庄','4321432',20,30,32,18,'治疗感冒',3,0,'admin','2019-03-15 15:27:27','admin','2019-03-15 15:27:27'),(3,'阿莫西林','321365468798','阿莫西林','6/2','石药','石家庄','20190316',15,50,43,7,'消炎',2,0,'三毛','2019-03-15 19:51:22','三毛','2019-03-15 19:51:22'),(4,'西瓜霜','8795649865165','西瓜霜','9/1','哈药','哈尔滨','20190305',12,20,11,5,'止咳',1,0,'三毛','2019-03-15 19:53:14','三毛','2019-03-15 19:53:14');

/*Table structure for table `t_drug_category` */

DROP TABLE IF EXISTS `t_drug_category`;

CREATE TABLE `t_drug_category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT COMMENT '药品类别id',
  `categoryName` varchar(20) NOT NULL COMMENT '药品类别名',
  `note` varchar(500) NOT NULL COMMENT '备注',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `t_drug_category` */

insert  into `t_drug_category`(`categoryId`,`categoryName`,`note`) values (1,'咳嗽药','主要治疗咳嗽的一些药物fds '),(2,'消炎药','处方药,不能和鹤顶红一起服用'),(3,'胶囊','处方药'),(5,'口腔类','治疗口腔溃疡疗效好');

/*Table structure for table `t_drug_sales` */

DROP TABLE IF EXISTS `t_drug_sales`;

CREATE TABLE `t_drug_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '销售id',
  `documentNo` varchar(32) DEFAULT NULL COMMENT '销售单据号',
  `inventoryQuantity` int(11) DEFAULT NULL COMMENT '销售数量',
  `price` float DEFAULT NULL COMMENT '单价',
  `inventory` float DEFAULT NULL COMMENT '销售金额',
  `storageTime` datetime DEFAULT NULL COMMENT '销售时间',
  `drugName` varchar(20) NOT NULL COMMENT '药品编号',
  `customer_id` int(11) NOT NULL COMMENT '客户id',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `F_customer_sales` (`customer_id`),
  CONSTRAINT `F_customer_sales` FOREIGN KEY (`customer_id`) REFERENCES `t_customer` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `t_drug_sales` */

insert  into `t_drug_sales`(`id`,`documentNo`,`inventoryQuantity`,`price`,`inventory`,`storageTime`,`drugName`,`customer_id`,`is_delete`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (2,'5469874561101',10,20,200,'2019-03-15 15:32:22','西山湖',1,0,'chenzhifu','2019-03-15 15:32:22','chenzhifu','2019-03-15 15:32:22'),(3,'12345678912',1,30,30,'2019-03-15 19:10:46','感冒胶囊',2,0,'wanglong','2019-03-15 19:10:46','wanglong','2019-03-15 19:10:46'),(4,'32136546879824',2,50,100,'2019-03-15 19:59:04','阿莫西林',4,0,'三毛','2019-03-15 19:59:04','三毛','2019-03-15 19:59:04'),(5,'123456789124',12,30,360,'2019-03-15 19:59:13','感冒胶囊',4,0,'三毛','2019-03-15 19:59:13','三毛','2019-03-15 19:59:13'),(6,'546987456154',5,20,100,'2019-03-15 19:59:27','西山湖',4,0,'三毛','2019-03-15 19:59:27','三毛','2019-03-15 19:59:27'),(7,'879564986516554',5,20,100,'2019-03-15 19:59:27','西瓜霜',4,0,'三毛','2019-03-15 19:59:27','三毛','2019-03-15 19:59:27'),(8,'32136546879854',5,50,250,'2019-03-15 19:59:39','阿莫西林',4,0,'三毛','2019-03-15 19:59:39','三毛','2019-03-15 19:59:39'),(9,'12345678954',5,30,150,'2019-03-15 19:59:39','感冒胶囊',4,0,'三毛','2019-03-15 19:59:39','三毛','2019-03-15 19:59:39'),(10,'5469874561210',2,20,40,'2019-03-15 20:14:08','西山湖',10,0,'烦啦烦啦','2019-03-15 20:14:08','烦啦烦啦','2019-03-15 20:14:08');

/*Table structure for table `t_drug_stock` */

DROP TABLE IF EXISTS `t_drug_stock`;

CREATE TABLE `t_drug_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '进货id',
  `documentNo` varchar(32) DEFAULT NULL COMMENT '入库单据号',
  `inventoryQuantity` int(11) DEFAULT NULL COMMENT '入库数量',
  `price` float DEFAULT NULL COMMENT '入库单价',
  `inventory` float DEFAULT NULL COMMENT '入库金额',
  `storageTime` datetime DEFAULT NULL COMMENT '入库时间',
  `drug_id` int(11) NOT NULL COMMENT '药品编号',
  `supplier_id` int(11) NOT NULL COMMENT '供货商id',
  `employees_id` int(11) NOT NULL COMMENT '员工id---经办人',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `F_drug_purchase` (`drug_id`),
  KEY `F_supplier_purchase` (`supplier_id`),
  KEY `F_employees_purchase` (`employees_id`),
  CONSTRAINT `F_drug_purchase` FOREIGN KEY (`drug_id`) REFERENCES `t_drug` (`id`),
  CONSTRAINT `F_employees_purchase` FOREIGN KEY (`employees_id`) REFERENCES `t_employees` (`uid`),
  CONSTRAINT `F_supplier_purchase` FOREIGN KEY (`supplier_id`) REFERENCES `t_supplier` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_drug_stock` */

insert  into `t_drug_stock`(`id`,`documentNo`,`inventoryQuantity`,`price`,`inventory`,`storageTime`,`drug_id`,`supplier_id`,`employees_id`,`is_delete`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'1574874654',50,12,600,'2019-03-01 00:00:00',1,2,2,0,'chenzhifu','2019-03-14 20:18:05','chenzhifu','2019-03-14 20:18:05'),(2,'20190315',50,15,12,'2019-03-20 00:00:00',2,1,3,0,'三毛','2019-03-15 19:56:22','三毛','2019-03-15 19:56:22');

/*Table structure for table `t_employees` */

DROP TABLE IF EXISTS `t_employees`;

CREATE TABLE `t_employees` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(36) DEFAULT NULL COMMENT '盐值',
  `gender` int(11) DEFAULT NULL COMMENT '性别,0-女性，1-男性',
  `age` int(4) DEFAULT NULL COMMENT '年龄',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `avatar` varchar(50) DEFAULT NULL COMMENT '头像',
  `cardBank` varchar(50) DEFAULT NULL COMMENT '开户行',
  `card` varchar(20) DEFAULT NULL COMMENT '银行账户',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `permissions` int(4) DEFAULT NULL COMMENT '权限，0-老板，1-员工',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `t_employees` */

insert  into `t_employees`(`uid`,`username`,`password`,`salt`,`gender`,`age`,`phone`,`email`,`avatar`,`cardBank`,`card`,`is_delete`,`permissions`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'ChenlLaoBan','2C187287BA1C3C893B9394FEA5B01F40','7035e18c-de0f-4e6c-9895-0e217ff34d10',1,23,'15133480838','15133480838@qq.com',NULL,'张家口','4569852367459874561',0,0,'chenzhifu','2019-03-14 20:04:36','chenzhifu','2019-03-14 20:04:36'),(2,'admin','24BAAA236EBEAB33C1D88856A6C2C436','cfdd7312-2fb1-4e6a-a657-597328d7627a',1,23,'15133330551','123',NULL,'石家庄','4569631478523214569',0,1,'chenzhifu','2019-03-14 20:07:50','chenzhifu','2019-03-14 20:07:50'),(3,'死啦死啦','FE2E66F2F6407196CC9AC4F34F2C3F8C','33a665cf-8849-4e4b-b2aa-7474554d5206',1,23,'12345678910','123456@qq.com',NULL,'农业银行','54648979856632154125',0,1,'老中医','2019-03-15 19:38:00','老中医','2019-03-15 19:38:00'),(4,'小白','B8824E86EEF770A4B3B4555C680A388D','495b7bc8-2688-4bbb-8905-8fa50ef62427',1,23,'13315960697','12434534@qq.com',NULL,'中国银行','45698765498765465465',0,1,'三毛','2019-03-15 19:48:30','三毛','2019-03-15 19:48:30');

/*Table structure for table `t_log_operation` */

DROP TABLE IF EXISTS `t_log_operation`;

CREATE TABLE `t_log_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) NOT NULL COMMENT '操作人的名',
  `operation` varchar(20) DEFAULT NULL COMMENT '操作',
  `table_name` varchar(20) DEFAULT NULL COMMENT '表名',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `note` varchar(200) NOT NULL COMMENT '详细操作',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_log_operation` */

/*Table structure for table `t_sales_return` */

DROP TABLE IF EXISTS `t_sales_return`;

CREATE TABLE `t_sales_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customerName` varchar(20) NOT NULL COMMENT '客户名称',
  `drugName` varchar(50) NOT NULL COMMENT '药品名称',
  `salesOrder` varchar(30) NOT NULL COMMENT '销售单号',
  `returnOrder` varchar(30) DEFAULT NULL COMMENT '退货单号',
  `drugAddress` varchar(200) DEFAULT NULL COMMENT '药品产地',
  `returnTime` datetime DEFAULT NULL COMMENT '退货时间',
  `employeesName` varchar(50) DEFAULT NULL COMMENT '员工名称',
  `drugPrice` float DEFAULT NULL COMMENT '购买药品单价',
  `number` int(11) DEFAULT NULL COMMENT '退货数量',
  `returnPrice` float DEFAULT NULL COMMENT '退货时金额',
  `amount` float DEFAULT NULL COMMENT '退货总金额',
  `why` varchar(500) DEFAULT NULL COMMENT '退货原因',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_sales_return` */

/*Table structure for table `t_stock_return` */

DROP TABLE IF EXISTS `t_stock_return`;

CREATE TABLE `t_stock_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `drugName` varchar(50) NOT NULL COMMENT '药品名称',
  `stockOrder` varchar(30) NOT NULL COMMENT '进货单号',
  `returnOrder` varchar(30) DEFAULT NULL COMMENT '退货单号',
  `drugAddress` varchar(200) DEFAULT NULL COMMENT '药品产地',
  `returnTime` datetime DEFAULT NULL COMMENT '退货时间',
  `employeesName` varchar(50) DEFAULT NULL COMMENT '员工名称',
  `drugPrice` float DEFAULT NULL COMMENT '购买药品单价',
  `number` int(11) DEFAULT NULL COMMENT '退货数量',
  `returnPrice` float DEFAULT NULL COMMENT '退货时金额',
  `amount` float DEFAULT NULL COMMENT '退货总金额',
  `why` varchar(500) DEFAULT NULL COMMENT '退货原因',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_stock_return` */

insert  into `t_stock_return`(`id`,`drugName`,`stockOrder`,`returnOrder`,`drugAddress`,`returnTime`,`employeesName`,`drugPrice`,`number`,`returnPrice`,`amount`,`why`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'西山湖','20190305','20190315','石家庄','2019-03-15 00:00:00','死啦死啦',15,2,15,30,'死啦','三毛','2019-03-15 19:57:42',NULL,NULL);

/*Table structure for table `t_supplier` */

DROP TABLE IF EXISTS `t_supplier`;

CREATE TABLE `t_supplier` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '供货商id',
  `username` varchar(20) NOT NULL COMMENT '厂名',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `cardBank` varchar(50) DEFAULT NULL COMMENT '开户行',
  `card` varchar(20) DEFAULT NULL COMMENT '银行账户',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除，0-未删除，1-已删除',
  `created_user` varchar(20) DEFAULT NULL COMMENT '创建执行人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` varchar(20) DEFAULT NULL COMMENT '修改执行人',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_supplier` */

insert  into `t_supplier`(`uid`,`username`,`phone`,`email`,`address`,`cardBank`,`card`,`is_delete`,`created_user`,`created_time`,`modified_user`,`modified_time`) values (1,'杨盼','17745630215','17745630215@qq.com','河北省石家庄市桥西区','石家庄桥西区支行','4523145698745234591',0,'杨盼','2019-03-14 20:10:22','杨盼','2019-03-14 20:10:22'),(2,'杨茜茜','12315678948','12315678948@qq.com','河北省张家口市','石家庄市桥西区','1234567894567891234',0,'杨茜茜','2019-03-14 20:11:35','杨茜茜','2019-03-14 20:11:35'),(3,'石药','12548798544','12548798544@xx.com','石家庄','农行','1234567891234567891',0,'石药','2019-03-15 19:55:10','石药','2019-03-15 19:55:10');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
