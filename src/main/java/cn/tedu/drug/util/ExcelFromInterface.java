package cn.tedu.drug.util;

import java.util.List;

/**
 * 表格导出的接口
 * @author PHP
 *
 */
public interface ExcelFromInterface {
	public void excel(String title,String[] rowName,List<Object[]>list,String url);
}
