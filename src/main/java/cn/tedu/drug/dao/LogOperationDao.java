package cn.tedu.drug.dao;

import java.util.List;
import java.util.Map;

import cn.tedu.drug.entity.LogOperation;

/**
 * 日志
 * @author PHP
 *
 */
public interface LogOperationDao {

	/**
	 * 根据条件，查询日志信息
	 */
	List<LogOperation> selectLogOperationForPage(Map<String,Object> map);
	/**
	 * 根据条件，查询日志信息数量
	 */
	Long selectLogOperationCount(Map<String,Object> map);
	
	
	
}
